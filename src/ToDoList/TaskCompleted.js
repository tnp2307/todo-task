import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "./Component/Button";
import { Table, Th, Thead, Tr } from "./Component/Table";

class TaskCompleted extends Component {
  renderDoneTable = () => {
    return this.props.toDoList
      .filter((task) => task.done == true)
      .map((task) => {
        return (
          <Tr>
            <Th valign="middle">{task.name}</Th>
            <Th className="text-right">
              <Button onClick={() => this.props.handleDeleteTask(task.id)}>
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  render() {
    return (
      <Table>
        <Thead>{this.renderDoneTable()}</Thead>
      </Table>
    );
  }
}
const mapDispatchTopProp = (dispatch) => {
  return {
    handleDeleteTask: (content) => {
      dispatch({ type: "DELETE_TASK", payload: content });
    },
  };
};
export default connect(null, mapDispatchTopProp)(TaskCompleted);
