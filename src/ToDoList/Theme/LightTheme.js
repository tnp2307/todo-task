export const ToDoListLightTheme={
    bgColor:'#F0F0F0',
    color:'#242C39',
    border:'1px solid #242C39',
    borderRadius:'none',
    hoverTextColor:'#F0F0F0',
    hoverBgColor:'#242C39',
    hoverBoder: '1px solid #F0F0F0'
}