export const ToDoListPrimaryTheme={
    bgColor:'#0D369F',
    color:'#F0F0F0',
    border:'1px solid #F0F0F0',
    borderRadius:'none',
    hoverTextColor:'#0D369F',
    hoverBgColor:'#F0F0F0',
    hoverBoder: '1px solid #0D369F'
}