export const ToDoListDarkTheme={
    bgColor:'#242C39',
    color:'#F0F0F0',
    border:'1px solid #fff',
    borderRadius:'none',
    hoverTextColor:'#242C39',
    hoverBgColor:'#F0F0F0',
    hoverBoder: '1px solid #242C39',
}