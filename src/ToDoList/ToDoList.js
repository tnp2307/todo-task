import React, { Component } from "react";
import { connect } from "react-redux";
import { ThemeProvider } from "styled-components";
import { Button } from "./Component/Button";
import { Dropdown } from "./Component/DropDown";
import { Heading1, Heading2 } from "./Component/Heading";
import { Table, Th, Thead, Tr } from "./Component/Table";
import { TextField } from "./Component/TextField";
import { Container } from "./Container/Container";
import { themeArr } from "./Redux/Reducer/ThemeReducer";
import TaskCompleted from "./TaskCompleted";
import TaskToDo from "./TaskToDo";
import { ToDoListDarkTheme } from "./Theme/DarkTheme";
import { ToDoListLightTheme } from "./Theme/LightTheme";
import { ToDoListPrimaryTheme } from "./Theme/PrimaryTheme";

class ToDoList extends Component {
  state = {
    input: "",
    preInput: "",
  };
  renderTheme = () => {
    return themeArr.map((theme) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };
  g;
  componentWillReceiveProps(newProps) {
    this.setState({
      input: newProps.taskEdit.name,
    });
  }
  render() {
    return (
      <div>
        <ThemeProvider theme={this.props.theme}>
          <Container>
            <Dropdown
              onChange={(e) => {
                this.props.handleChangeTheme(e.target.value);
              }}
              className="m-2"
            >
              {this.renderTheme()}
            </Dropdown>
            <Heading2>To do list</Heading2>
            <TextField
              value={this.state.input}
              onChange={(e) => {
                this.setState({
                  input: e.target.value,
                });
              }}
              label={"Task name"}
            />
            <Button
              onClick={(e) => {
                this.props.handleAddTask(this.state.input);
              }}
              className="mx-2"
            >
              Add task
            </Button>
            <Button
              onClick={() => this.props.handleUpdateTask(this.state.input)}
              className="mx-2"
            >
              Update task
            </Button>
            <Heading2>Task to do</Heading2>
            <TaskToDo toDoList={this.props.toDoList} />
            <Heading2>Task complete</Heading2>
            <TaskCompleted toDoList={this.props.toDoList} />
          </Container>
        </ThemeProvider>
      </div>
    );
  }
}
const mapStateToProp = (state) => {
  return {
    theme: state.themeReducer.theme,
    toDoList: state.taskReducer.taskList,
    taskEdit: state.taskReducer.taskEdit,
  };
};
const mapDispatchTopProp = (dispatch) => {
  return {
    handleAddTask: (content) => {
      dispatch({ type: "ADD_TASK", payload: content });
    },
    handleUpdateTask: (content) => {
      dispatch({ type: "UPDATE_TASK", payload: content });
    },
    handleChangeTheme: (theme) => {
      dispatch({ type: "CHOOSE_THEME", payload: theme });
    },
  };
};
export default connect(mapStateToProp, mapDispatchTopProp)(ToDoList);
