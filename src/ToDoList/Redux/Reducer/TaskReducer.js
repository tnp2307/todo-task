const initialState = {
  taskList: [
    { id: 1, name: "Task 1", done: false },
    { id: 2, name: "Task 2", done: false },
    { id: 3, name: "Task 3", done: false },
    { id: 4, name: "Task 4", done: false },
    { id: 5, name: "Task 5", done: true },
    { id: 6, name: "Task 6", done: true },
  ],
  taskEdit: { id: "", name: "", done: false },
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case "ADD_TASK": {
      let index = state.taskList.findIndex((task) => task.name == payload);
      if (payload == "") {
        alert("Please input task");
        return { ...state };
      } else if (index != -1) {
        alert(`Task already exist - ${state.taskList[index].name} `);

        return { ...state };
      }
      let id = state.taskList.length + 1;
      let newTask = { name: payload, id: id, done: false };
      let cloneTaskList = [...state.taskList];
      cloneTaskList.push(newTask);
      return { ...state, taskList: cloneTaskList };
    }
    case "DELETE_TASK": {
      let cloneTaskList = [...state.taskList];

      let index = cloneTaskList.findIndex((task) => task.name == payload);

      cloneTaskList.splice(index, 1);
      return { ...state, taskList: cloneTaskList };
    }
    case "ADJUST_TASK": {
      return { ...state, taskEdit: payload };
    }
    case "UPDATE_TASK": {
      state.taskEdit = { ...state.taskEdit, name: payload };
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex(
        (task) => task.id == state.taskEdit.id
      );
      cloneTaskList[index].name=payload
      return { ...state, taskList:cloneTaskList };
    }
    case "FINISH_TASK": {
      let cloneTaskList = [...state.taskList];
      let index = cloneTaskList.findIndex((task) => task.name == payload);
      cloneTaskList[index].done = true;

      return { ...state, taskList: cloneTaskList };
    }
    default:
      return state;
  }
};
