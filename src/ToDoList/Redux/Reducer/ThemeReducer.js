import { ToDoListDarkTheme } from "../../Theme/DarkTheme";
import { ToDoListLightTheme } from "../../Theme/LightTheme";
import { ToDoListPrimaryTheme } from "../../Theme/PrimaryTheme";

export const themeArr = [
  { id: 1, name: "Dark Theme", theme: ToDoListDarkTheme },
  { id: 2, name: "Light Theme", theme: ToDoListLightTheme },
  { id: 3, name: "Primary Theme", theme: ToDoListPrimaryTheme },
];

const initialState = {
  theme: ToDoListDarkTheme,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case "CHOOSE_THEME":
      let theme = themeArr.find((theme) => theme.id == payload);
      if (theme) {
        state.theme = theme.theme;
      }
      console.log(state.theme);
      return { ...state };

    default:
      return state;
  }
};
