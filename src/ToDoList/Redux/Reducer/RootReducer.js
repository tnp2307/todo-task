import { combineReducers } from "redux";
import TaskReducer from "./TaskReducer";
import ThemeReducer from "./ThemeReducer";

export const rootReducer = combineReducers({
  taskReducer: TaskReducer,
  themeReducer: ThemeReducer,
});
