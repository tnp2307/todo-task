import styled from "styled-components";

export const Container = styled.div`
width: 80%;
padding :20px 20px;
margin-right: auto;
margin-left: auto;
color:${props=>props.theme.color};
background-color:${props=>props.theme.bgColor};

`;
