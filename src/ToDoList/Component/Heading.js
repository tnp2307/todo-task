import styled from "styled-components";
export const Heading1 = styled.h1`
  margin-bottom: 0.5rem;
  font-family: inherit;
  font-weight: 500;
  line-height: 1.2;
  color: ${(props) => props.theme.color};
  font-size: 2.5rem;
`;
export const Heading2 = styled.h1`
  margin-bottom: 0.5rem;
  font-family: inherit;
  font-weight: 500;
  line-height: 1.2;
  color: ${(props) => props.theme.color};
  font-size: 2rem;
`;
export const Heading3 = styled.h1`
  margin-bottom: 0.5rem;
  font-family: inherit;
  font-weight: 500;
  line-height: 1.2;
  color: ${(props) => props.theme.color};
  font-size: 1.75rem;
`;
export const Heading4 = styled.h1`
  margin-bottom: 0.5rem;
  font-family: inherit;
  font-weight: 500;
  line-height: 1.2;
  color: ${(props) => props.theme.color};
  font-size: 1.5rem;
`;
