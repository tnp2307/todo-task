import styled from "styled-components";
export const Dropdown = styled.select`
  width: 100%;
  height: 50px;
  font-size: 100%;
  font-weight: bold;
  border-radius: 0;
  background-color: ${(props) => props.theme.bgColor};
  color: ${(props) => props.theme.color};
  border: ${(props) => props.theme.border};
  transition: color 0.3s ease, background-color 0.3s ease,
    border-bottom-color 0.3s ease;
  &:hover {
    color: ${(props) => props.theme.hoverTextColor};
    background-color: ${(props) => props.theme.hoverBgColor};
    border-bottom-color: #dcdcdc;
  }
`;
