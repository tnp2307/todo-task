import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "./Component/Button";
import { Table, Th, Thead, Tr } from "./Component/Table";

class TaskToDo extends Component {
  renderToDoTable = () => {
    return this.props.toDoList
      .filter((task) => task.done == false)
      .map((task) => {
        return (
          <Tr>
            <Th valign="middle">{task.name}</Th>
            <Th className="text-right">
              <Button onClick={()=>this.props.handleAdjustTask(task)}>
                <i className="fa fa-edit"></i>
              </Button>
              <Button onClick={()=>this.props.handleFinishTask(task.name)}>
                <i className="fa fa-check"></i>
              </Button>
              <Button onClick={()=>this.props.handleDeleteTask(task.name)}>
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  render() {
    return (
      <Table>
        <Thead>{this.renderToDoTable()}</Thead>
      </Table>
    );
  }
}

const mapDispatchTopProp = (dispatch) => {
  return {
    handleDeleteTask: (content) => {
      dispatch({ type: "DELETE_TASK", payload: content });
    },
    handleAdjustTask: (content) => {
      dispatch({ type: "ADJUST_TASK", payload: content });
    },
    handleFinishTask: (content) => {
      dispatch({ type: "FINISH_TASK", payload: content });
    },
  };
};
export default connect(null, mapDispatchTopProp)(TaskToDo);
